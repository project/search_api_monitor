<?php

namespace Drupal\search_api_monitor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class MonitorController extends ControllerBase {

  public function monitorServers() {
    $servers = \Drupal::entityTypeManager()
      ->getStorage('search_api_server')
      ->loadByProperties(['status' => TRUE]);

    $data = [
      'servers' => [],
      'request_time' => \Drupal::time()->getRequestTime(),
    ];

    foreach ($servers as $server) {
      $label = $server->label();
      $is_available = $server->isAvailable();
      $data['servers'][$label] = $is_available ? 'available' : 'unavailable';
    }

    return new JsonResponse($data);
  }
}
